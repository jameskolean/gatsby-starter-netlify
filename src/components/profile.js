import React from 'react'
import { useIdentityContext } from 'react-netlify-identity-widget'

const Profile = () => {
  const identity = useIdentityContext()
  return (
    <section className="section section--gradient">
      <div className="container">
        <div className="columns">
          <div className="column is-10 is-offset-1">
            <div className="section">
              <h2 className="title is-size-3 has-text-weight-bold is-bold-light">
                Your profile
              </h2>
              <p>Full name: {identity.user.user_metadata.full_name}</p>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}
export default Profile
