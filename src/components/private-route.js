import React from 'react'
import { useIdentityContext } from 'react-netlify-identity-widget'
import { navigate } from 'gatsby'

const PrivateRoute = ({ component: Component, location, ...rest }) => {
  const identity = useIdentityContext()
  if (!identity.isLoggedIn && location.pathname !== `/app/login`) {
    navigate('/app/login')
    return null
  }
  return <Component {...rest} />
}
export default PrivateRoute
