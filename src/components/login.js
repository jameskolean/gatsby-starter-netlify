import React from 'react'
import { useIdentityContext } from 'react-netlify-identity-widget'

const Login = () => {
  const identity = useIdentityContext()

  return (
    <section className="section section--gradient">
      <div className="container">
        <div className="columns">
          <div className="column is-10 is-offset-1">
            <div className="section">
              <h2 className="title is-size-3 has-text-weight-bold is-bold-light">
                {identity && identity.isLoggedIn
                  ? 'You have successfully logged in.'
                  : 'You must log in to access protected pages.'}
              </h2>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}
export default Login
