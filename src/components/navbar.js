import React, { useState } from 'react'
import { Link } from 'gatsby'
import IdentityModal, {
  useIdentityContext
} from 'react-netlify-identity-widget'
import 'react-netlify-identity-widget/styles.css' // delete if you want to bring your own CSS

import logo from '../img/logo.svg'

const Navbar = () => {
  const identity = useIdentityContext()
  const [isIdentityModalOpen, setIdentityModalOpen] = React.useState(false)
  const getName = () => {
    return (
      (identity &&
        identity.user &&
        identity.user.user_metadata &&
        identity.user.user_metadata.full_name) ||
      'NoName'
    )
  }
  const [navState, setNavState] = useState({
    active: false,
    navBarActiveClass: ''
  })
  const isLoggedIn = identity && identity.isLoggedIn

  const toggleHamburger = () => {
    // toggle the active boolean in the state
    const toggled = !navState.active
    const navBarActiveClass = toggled ? 'is-active' : ''
    setNavState(state => {
      return {
        active: toggled,
        navBarActiveClass: navBarActiveClass
      }
    })
  }
  const openLogin = () => {
    toggleHamburger()
    setIdentityModalOpen(true)
  }

  return (
    <>
      <nav
        className="navbar is-transparent"
        role="navigation"
        aria-label="main-navigation"
      >
        <div className="container">
          <div className="navbar-brand">
            <Link to="/" className="navbar-item" title="Logo">
              <img src={logo} alt="Kaldi" style={{ width: '88px' }} />
            </Link>
            {/* Hamburger menu */}
            <button
              className={`navbar-burger burger ${navState.navBarActiveClass}`}
              data-target="navMenu"
              onClick={() => toggleHamburger()}
            >
              <span />
              <span />
              <span />
            </button>
          </div>
          <div
            id="navMenu"
            className={`navbar-menu ${navState.navBarActiveClass}`}
          >
            <div className="navbar-start has-text-centered">
              <Link className="navbar-item" to="/about">
                About
              </Link>
              <Link className="navbar-item" to="/products">
                Products
              </Link>
              <Link className="navbar-item" to="/blog">
                Blog
              </Link>
              <Link className="navbar-item" to="/contact">
                Contact
              </Link>
              <Link className="navbar-item" to="/contact/examples">
                Form Examples
              </Link>
              <Link className="navbar-item" to="/app/profile">
                Protected Profile
              </Link>
            </div>
            <div className="navbar-end has-text-centered">
              <button className="btn" onClick={() => openLogin()}>
                {isLoggedIn ? `Hello ${getName()}, Log out here!` : 'LOG IN'}
              </button>{' '}
            </div>
          </div>
        </div>
      </nav>
      <IdentityModal
        aria-label="test"
        showDialog={isIdentityModalOpen}
        onCloseDialog={() => setIdentityModalOpen(false)}
        onLogin={() => setIdentityModalOpen(false)}
      />
    </>
  )
}

export default Navbar
