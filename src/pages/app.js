import React from 'react'
import { Router } from '@reach/router'
import Layout from '../components/layout'
import Profile from '../components/profile'
import Login from '../components/login'
import PrivateRoute from '../components/private-route'
const App = () => (
  <Layout>
    <Router>
      <Login path="/app/login" />
      <PrivateRoute path="/app/profile" component={Profile} />
    </Router>
  </Layout>
)
export default App
